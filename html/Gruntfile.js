/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
      compass: {
          comus: {
              options: {
                  config: 'framework/config.rb'
              }
          },
          frontend: {
              options: {
                  config: 'sass/config.rb'
              }
          }
      },
      watch: {
          comus: {
              files: ["framework/*.scss", "framework/*/*.scss"],
              tasks: ["compass:comus"]
          },
          frontend: {
              files: ["sass/*.scss", "sass/*/*.scss"],
              tasks: ["compass:frontend"]
          }
      }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);
};
