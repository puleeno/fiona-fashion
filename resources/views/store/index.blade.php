@extends('master')
@section('title')
    Cửa hàng
@endsection
@section('content')
<main class="site-body" id="main">
    <div class="container">
        <div class="store-listings">
            <div class="store-container">
                <div class="top-content">
                    <h1 class="page-heading">Hệ thống cửa hàng của fiona trên toàn quốc</h1>
                    <div class="choose-locations">
                        <form ng-submit="submit()" ng-controller="StoreController">
                            <label>fiona ở các tỉnh thành</label>
                            <select>
                                <option value="">Chọn địa điểm</option>
                                @foreach($locations as $location)
                                <option value="{{$location->id}}">{{$location->name}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                </div>
                <div class="main-content">
                    <ul class="store-images">
                        <li>
                            <img src="/html/images/tmp/stores/p1.jpg" alt="">
                        </li>
                        <li>
                            <img src="/html/images/tmp/stores/p2.jpg" alt="">
                        </li>
                        <li>
                            <img src="/html/images/tmp/stores/p3.jpg" alt="">
                        </li>
                    </ul>
                    @if(!$stores->isEmpty())
                    <ol class="stores">
                        @foreach($stores as $store)
                        <li>
                            <h3 class="store-name">{{$store->name}}</h3>
                            <p class="address">{{$store->address}}</p>
                            <p class="hotline">Phone  & Hotline: <strong>{{$store->telephone}} - {{$store->hotline}}</strong></p>
                            <p class="website"><a href="{{$store->name}}">{{$store->website}}</a></p>
                            <p class="fanpage"><a href="{{$store->name}}">{{$store->facebook}}</a></p>
                        </li>
                        @endforeach
                    </ol>
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
