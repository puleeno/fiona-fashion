@extends('master')
@section('title')
    Danh sách cửa hàng
@endsection
@section('content')
<main class="site-body" id="main">
    <div class="container">
        <div class="store-listings">
            <div class="store-container">
                <div class="top-content">
                    <h1 class="page-heading">Hệ thống cửa hàng của fiona trên toàn quốc</h1>
                    <div class="choose-locations">
                        <form>
                            <label>fiona ở các tỉnh thành</label>
                            <select>
                                <option value="">Chọn địa điểm</option>
                            </select>
                        </form>
                    </div>
                </div>
                <div class="main-content">
                    <ol class="stores-with-map">
                        <li>
                            <div class="store-inner">
                                <div class="store-info">
                                    <h3 class="store-name">Show Room Big C Long Biên</h3>
                                    <p class="address">Gian hàng 20 - Big C Long Biên - TTTM Savico đường Nguyễn Văn Linh - Q. Long Biên - TP. Hà Nội</p>
                                    <p class="hotline">Phone  & Hotline: <strong>046 257 3221 - 09811 22 811</strong></p>
                                    <p class="website"><a href="http://fiona.com">http://fiona.com</a></p>
                                    <p class="fanpage"><a href="http://facebook.com/thoitrangfiona">http://facebook.com/thoitrangfiona</a></p>
                                </div>
                                <div class="store-map">
                                    <p>
                                        Chỉ dẫn đường đi trên <a href="#" title="">Google map</a>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="store-inner">
                                <div class="store-info">
                                    <h3 class="store-name">Show Room Big C Long Biên</h3>
                                    <p class="address">Gian hàng 20 - Big C Long Biên - TTTM Savico đường Nguyễn Văn Linh - Q. Long Biên - TP. Hà Nội</p>
                                    <p class="hotline">Phone  & Hotline: <strong>046 257 3221 - 09811 22 811</strong></p>
                                    <p class="website"><a href="http://fiona.com">http://fiona.com</a></p>
                                    <p class="fanpage"><a href="http://facebook.com/thoitrangfiona">http://facebook.com/thoitrangfiona</a></p>
                                </div>
                                <div class="store-map">
                                    <p>
                                        Chỉ dẫn đường đi trên <a href="#" title="">Google map</a>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="store-inner">
                                <div class="store-info">
                                    <h3 class="store-name">Show Room Big C Long Biên</h3>
                                    <p class="address">Gian hàng 20 - Big C Long Biên - TTTM Savico đường Nguyễn Văn Linh - Q. Long Biên - TP. Hà Nội</p>
                                    <p class="hotline">Phone  & Hotline: <strong>046 257 3221 - 09811 22 811</strong></p>
                                    <p class="website"><a href="http://fiona.com">http://fiona.com</a></p>
                                    <p class="fanpage"><a href="http://facebook.com/thoitrangfiona">http://facebook.com/thoitrangfiona</a></p>
                                </div>
                                <div class="store-map">
                                    <p>
                                        Chỉ dẫn đường đi trên <a href="#" title="">Google map</a>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="store-inner">
                                <div class="store-info">
                                    <h3 class="store-name">Show Room Big C Long Biên</h3>
                                    <p class="address">Gian hàng 20 - Big C Long Biên - TTTM Savico đường Nguyễn Văn Linh - Q. Long Biên - TP. Hà Nội</p>
                                    <p class="hotline">Phone  & Hotline: <strong>046 257 3221 - 09811 22 811</strong></p>
                                    <p class="website"><a href="http://fiona.com">http://fiona.com</a></p>
                                    <p class="fanpage"><a href="http://facebook.com/thoitrangfiona">http://facebook.com/thoitrangfiona</a></p>
                                </div>
                                <div class="store-map">
                                    <p>
                                        Chỉ dẫn đường đi trên <a href="#" title="">Google map</a>
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection