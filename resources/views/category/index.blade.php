@extends('master')
@section('title')
    {{$cat->name}}
@endsection
@section('content')
<main class="site-content" id="main">
    <div class="container">
        @if(!$cat->products()->get()->isEmpty())
        <div class="products-page" itemscope itemtype="http://schema.org/ItemList">
            @foreach($cat->products()->get() as $product)
            <div class="product">
                <div class="product-inner">
                    @if(!$product->images()->get()->isEmpty())
                    <img src="{{ URL::to('core/images/product/' . $product->images()->first()->folder . $product->images()->first()->stored_file_name) }}" alt="{{$product->name}}">
                    @endif
                    @if($product->old_price > 0 && $product->old_price > $product->price)
                    <?php
                    $discount = round(100 - ($product->price / $product->old_price * 100));
                    ?>
                    <div class="saleoff">Sale {{$discount}}%</div>
                    @endif
                    <div class="overlay" ng-click="viewProduct()">
                        <div class="overlay-inner">
                            <h2 class="product-name">{{ $product->name }}</h2>
                            <p class="product-sku">{{ $product->sku }}</p>
                            <span class="hr"></span>
                            <p class="product-price">
                                {{ number_format($product->price, 0, ',', '.')}} VND
                            </p>
                            <a href="" title="{{$product->name}}">Chi tiết <span class="screen-reader-text">{{$product->name}} {{ $product->sku }}</span></a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="no-content">
            Hiện tại chúng tôi chưa có sản phẩm nào!
        </div>
        @endif
        <div class="clearfix"></div>
    </div>
</main>
@endsection
