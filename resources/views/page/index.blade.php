@extends('master')
@section('title')
    Trang chủ
@endsection
@section('content')
<div class="site-body">
    <div class="container">
        <main id="main" class="site-content">
            {!! $page->content !!}
        </main>
        <aside class="sidebar primary-sidebar">
            <nav class="sidebar-navigation">
                @if (!empty($page_nav))
                <ul>
                    @foreach ($page_nav as $nav)
                    <li>
                        <a href="{{$nav->alias_title}}.html" class="{{$nav->alias_title}}@if(Request::is($nav->alias_title . '.html'))
                        active
                        @endif">{{ $nav->title }}</a>
                    </li>
                    @endforeach
                </ul>
                @endif
            </nav>
        </aside>
        <div class="clearfix"></div>
    </div>
</div>
@stop
