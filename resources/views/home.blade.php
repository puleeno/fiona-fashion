@extends('master')
@section('title')
    Trang chủ
@endsection
@section('content')
<main class="site-content" id="main" >
    <div class="container">
        @if(!$categories->isEmpty())
        <section class="featured-news">
            <h3 class="screen-reader-text">Sản phẩm nổi bật</h3>
            <div class="leftBock">
                <div class="first-news">
                    @if(isset($categories[0]))
                    <a href="{{URL::to('danh-muc/' . $categories[0]->alias . '.html')}}" title="{{$categories[0]->name}}" class="news-inner">
                        <img src="assets/images/tmp/home/p1.png" alt="{{$categories[0]->name}}">
                        <div class="overlay">
                            <div class="overlay-inner">
                                <span class="btn btn-detail">Xem ngay</span>
                            </div>
                        </div>
                    </a>
                    @endif
                </div>
                <ul class="others-news">
                    @if(isset($categories[1]))
                    <li class="news">
                        <a href="{{URL::to('danh-muc/' . $categories[1]->alias . '.html')}}" title="{{$categories[1]->name}}" class="news-inner">
                            <img src="assets/images/tmp/home/p2.jpg" alt="{{$categories[1]->name}}">
                            <div class="overlay">
                                <div class="overlay-inner">
                                    <span class="btn btn-detail">Xem ngay</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    @endif
                    @if(isset($categories[2]))
                    <li class="news">
                        <a href="{{URL::to('danh-muc/' . $categories[2]->alias . '.html')}}" title="{{$categories[2]->name}}" class="news-inner">
                            <img src="assets/images/tmp/home/p3.jpg" alt="{{$categories[2]->name}}">
                            <div class="overlay">
                                <div class="overlay-inner">
                                    <span class="btn btn-detail">Xem ngay</span>
                                </div>
                            </div>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
            @if(isset($categories[3]))
            <div class="rightBlock">
                <a href="{{URL::to('danh-muc/' . $categories[3]->alias . '.html')}}" title="{{$categories[3]->name}}" class="news-inner">
                    <img src="assets/images/tmp/home/p4.jpg" alt="{{$categories[3]->name}}">
                    <div class="overlay">
                        <div class="overlay-inner">
                            <span class="btn btn-detail">Xem ngay</span>
                        </div>
                    </div>
                </a>
            </div>
            @endif
        </section>
        @else
        <div class="no-content">Chưa có sản thông tin nào trong hệ thống</div>
        @endif
    </div>
</main>
@endsection
