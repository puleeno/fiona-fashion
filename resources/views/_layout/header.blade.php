<header class="site-header" itemscope="" itemtype="http://schema.org/WPHeader" ng-controller="MasterController">
    <div class="container">
        <span class="mobile-menu-button" ng-click="mobileMenu()"></span>
        <span class="border"></span>
        <h1 id="logo">
            <a href="{{URL::to('')}}">Fiona</a>
        </h1>

        <nav class="cm-navigation primary-navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement" aria-label="Main navigation">
            <ul>
                <li>
                    <a href="{{URL::to('gioi-thieu.html')}}" class="gioi-thieu @if(Request::is('gioi-thieu.html')) active @endif">Về Fiona</a>
                </li>
                <li>
                    <a href="{{ URL::to('san-pham.html') }}" class="san-pham @if(Request::is('san-pham.html')) active @endif">Cửa hàng</a>
                </li>
                <li>
                    <a href="{{ URL::to('tin-tuc.html') }}" class="tin-tuc @if(Request::is('tin-tuc.html')) active @endif">Tin tức</a>
                </li>
                <li class="cm-account">
                    @if (\Auth::guard('customer')->user())
                        Xin chào {{\Auth::guard('customer')->user()->fullname}}
                    @else
                        <a href="" ng-click="open()">
                            <span class="icon"></span>
                            <span class="text">Đăng nhập/Đăng ký</span>
                        </a>
                    @endif
                </li>
                <li class="cm-cart">
                    <a href="" class="gio-hang" ng-click="viewCarts()">
                        <span class="screen-reader-text">Giỏ hàng</span>
                        <span class="counter">@{{numberItem}}</span>
                    </a>
                </li>
                <li class="search-header">
                    <form>
                        <div class="input-text">
                            <span class="icon"></span>
                            <input class="text" type="text" placeholder="Tìm kiếm"></div>
                    </form>
                </li>
            </ul>
        </nav>
        <div class="clearfix"></div>
    </div>
</header>
<nav class="sub-navigation" itemscope="" itemtype="http://schema.org/SiteNavigationElement" aria-label="Secondary navigation">
    <div class="container">
        <ul>
            @foreach($product_cats->where('id','!=', 1)->where('parent_id', 1)->get() as $cat)
            <li>
                <a href="{{URL::to('danh-muc/' . $cat->alias . '.html')}}" title="{{ $cat->name }}">{{ $cat->name }}</a>
                @if(!$product_cats->where('parent_id', $cat->id)->get()->isEmpty())
                <ul>
                    @foreach($product_cats->where('parent_id', $cat->id)->get() as $sub_cat)
                    <li>
                        <a href="{{URL::to('danh-muc/' . $sub_cat->alias . '.html')}}" title="{{ $cat->name }}">{{ $sub_cat->name }}</a>
                    </li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach;
            <li><a href="{{ URL::to('bo-suu-tap.html') }}" title="Bộ sưu tập">Bộ sưu tập</a></li>
            <li><a href="{{ URL::to('khuyen-mai.html') }}" title="Safeoff">Saleoff</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
</nav>
<script type="text/javascript">
    window.carts = {!! json_encode(getCarts()) !!};
    window.numberItem = {!! json_encode(getNumberItem()) !!};
</script>
<script type="text/javascript" src="/assets/js/master.js"></script>
