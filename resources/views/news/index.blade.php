@extends('master')
@section('title')
Tin tức
@endsection
@section('content')

<main class="site-content" id="main">
    <div class="container">
        <div class="news-page">
            @if(!$articles->isEmpty())
            @foreach($articles as $article)
            <?php $image = $article->images()->first(); ?>
            @if($start_index == 0)
            <ul class="top-news">
            @endif
            <li class="news">
                <a href="{{URL::to('tin-tuc/' . $article->alias_title . '.html')}}" class="news-inner" title="{{$article->title}}">
                    @if($image) <img src="{{ URL::to('core/images/news/' . $image->folder . '/' . $image->store_file_name) }}" alt="{{$article->title}}"> @endif
                    <div class="overlay">
                        <div class="overlay-inner">
                            <span class="btn btn-detail">Xem ngay</span>
                            <div class="title">{{$article->title}}</div>
                        </div>
                    </div>
                </a>
                {{$start_index}}
            </li>
            @if($start_index == 1 || $total_articles == 1)
            </ul>
                @if($total_articles > 2)
                    <ol class="bottom-news">
                @endif
            @endif
            @if($start_index == $total_articles - 1 && $total_articles > 2)
                </ol>
            @endif
            <?php $start_index++; ?>
            @endforeach
            @else
            <div class="no-content">
                Chưa có tin tức nào trong dữ liệu của chúng tôi.
            </div>
            @endif
            <div class="clearfix"></div>
        </div>
    </div>
</main>

@stop
