@extends('master')
@section('title')
{{$article->title}}
@stop
@section('content')
<main class="site-body" id="main">
    <div class="container">
        <div class="news-page detail-page">
            <h1 class="page-title">{{$article->title}}</h1>
            <div class="meta">
                <p class="date">{{ date('d/m/Y - H:i:s', strtotime($article->created_at)) }}</p>
            </div>
            <div class="content">
                {!! $article->content !!}
            </div>

            <div class="social-sharing">
                <ul>
                    <li class="facebook"><div class="fb-like" data-href="{{Request::url()}}" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div></li>
                </ul>
            </div>
        </div>
        @if(!$related_articles->isEmpty())
        <div class="related-post">
            <h3 class="sub-page-heading">Những bài viết liên quan</h3>
            <ul class="related">
                @foreach($related_articles as $article)
                <li>
                    <a href="{{ URL::to('tin-tuc/' . $article->alias_title . '.html' )}}" title="{{$article->title}}">{{$article->title}}</a>
                    <p class="date">{{ date('d/m/Y - H:i:s', strtotime($article->created_at)) }}</p>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</main>
@stop
