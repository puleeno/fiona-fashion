<!DOCTYPE html>
<html lang="vi" ng-app="fiona">
<head itemscope="" itemtype="http://schema.org/WebSite">
    <title>@yield('title') | Fiona Fashion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/assets/css/comus.css">
    <link rel="stylesheet" href="/bower_components/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/bower_components/ng-dialog/css/ngDialog.css">
	<link rel="stylesheet" href="/bower_components/ng-dialog/css/ngDialog-theme-default.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <script type="text/javascript" src="/bower_components/angular/angular.min.js"></script>
</head>
<body class="home" itemscope itemtype="http://schema.org/WebPage" ng-controller="MainCtrl">
    @include('_layout.header')

    @yield('content')

    @include('_layout.footer')
    <script type="text/javascript">
    WebFontConfig = {
        google: { families: [ 'Roboto:400,300,100,100italic,300italic,400italic,500,500italic,700,900:latin,vietnamese' ] }
    };
    (function() {
        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
    </script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.7&appId=258520054510523";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script type="text/javascript">
    var fiona_url = '{{URL::to('/')}}';
    </script>
    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="/bower_components/ng-dialog/js/ngDialog.min.js"></script>
    <script type="text/javascript" src="/assets/js/angularlike.js"></script>
    <script type="text/javascript" src="/assets/js/fiona.js"></script>
</body>
    @include('shared.script')
</html>
