<?php

use Illuminate\Database\Seeder;
use Comus\Core\Database\UserTableSeeder as UserTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
    }
}
