
var defaultModules = 
[
    'ui.bootstrap',
    'ngResource',
    'xeditable',
    'ngTable',
    'ngImgCrop',
    'angularFileUpload',
    'user',
    'role',
    'permission'
];

if(typeof modules != 'undefined'){
	defaultModules = defaultModules.concat(modules);
}

var myApp = angular.module('app', defaultModules);
