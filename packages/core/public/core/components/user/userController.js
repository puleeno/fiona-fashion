var userModule = angular.module('user');
userModule.controller('UserController', ['$scope', '$parse', 'UserService', '$modal', 'ngTableParams', '$timeout', '$filter', function ($scope, $parse, UserService, $modal, ngTableParams, $timeout, $filter) {
    
    $scope.user = {};

    $scope.isSearch = false;
    
    if(typeof window.dataController != 'undefined'){
        for(var key in window.dataController){
            var model = $parse(key);
            model.assign($scope, window.dataController[key]);
        }
    }

    $scope.$watch('hashData', function(){
        UserService.setHashData($scope.hashData);
    });

    $scope.updatePermission = function(){
        UserService.updatePermissions($scope.id, Object.keys($scope.userPermissions));
    }

    $scope.updateRole = function(){
        UserService.updateRoles($scope.id, Object.keys($scope.userRoles));
    }

    $scope.isAction = false;
    $scope.items_s = []; //UserService.setData(angular.copy($scope.items));
    $timeout(function(){
         angular.element('#filter-checkbox').trigger('click');
         angular.element('#filter-checkbox-contact').trigger('click');
     },500)

    $scope.changeFilter = function(){
        $timeout(function(){
            $scope.isAction = !$scope.isAction;
        },1000)
       
    }
    $scope.showGroup = function(event){
        // event.preventDefault();
        $(event.target).parent().toggleClass("ac-up");
        $(document).on('click', function closeMenu (e){
            $(e.target).closest('tr').siblings().find('.wrap-ac-group').removeClass('ac-up');
            if($('.wrap-ac-group').has(e.target).length === 0){
                $('.wrap-ac-group').removeClass('ac-up');
            } else {
                $(document).one('click', closeMenu);
            }
        });
        angular.element('.table-responsive').addClass('fix-height');
    }
    UserService.query().then(function(data){

        $scope.items_s = UserService.getUsers();
        $scope.tableParams = new ngTableParams({
            page: 1, // show first page
            count: 10, // count per page
            sorting: {
                'name': 'asc' // initial sorting
            },
        }, {
            total: $scope.items_s.length, // length of data
            getData: function($defer, params) {
                var orderedData = params.sorting() ? $filter('orderBy')($scope.items_s, params.orderBy()) : $scope.items_s;
                orderedData = params.filter() ? $filter('filter')(orderedData, params.filter()) : orderedData;
                params.total(orderedData.length);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
    })

    $scope.delete = function(id) {
        if (!confirm('Do you want delete this user')) return;
        UserService.remove(id).then(function(data) {
            $scope.items_s = UserService.getUsers();
            $scope.tableParams.reload();
        });
    }
            /**
     * [deleteModalPwv description]
     * @param  {[type]} item [description]
     * @return {[type]}      [description]
     */
    $scope.changeStatus = function(item,$index) {
          var modalInstance = $modal.open({
            templateUrl: '/app/components/user/views/modal/changeStatusUser.html',
            controller: 'ModalChangeStatusCtrl',
            size: undefined,
            resolve: {
                userInfo: function(){
                    return item;
                }
            }
          });
          
          modalInstance.result.then(function (result) {
              $scope.items_s = UserService.getUsers();
              $scope.tableParams.reload();
          }, function () {
          });
        
    };
    $scope.showContact = function(userId, isShowContact){
        UserService.showContact({id:userId, is_show_contact:isShowContact}).then(function(data){
            $scope.items_s = UserService.getUsers();
            $scope.tableParams.reload();
        });
    }
    $scope.convetDateTime = function(dateStr){
        if(dateStr == '0000-00-00 00:00:00') return '';
        // if(new Date(dateStr.replace(/-/g, "/")) =='Invalid Date') return '';
        var a=dateStr.split(" ");
        var d=a[0].split("-");
        var t=a[1].split(":");
        var date = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);

        return $filter('date')(date, 'yyyy-MM-dd HH:mm:ss');
    }
    /**
     * get layout update user
     * @param  {[int]} id id of user. it can null
     * @return {[type]}    [description]
     */
    $scope.getModalCreateUser = function(id) {
        var templateUrl = '/admin/user/create';
        if (typeof id != 'undefined') {
            templateUrl = '/admin/user/' + id + '/edit' + '?' + new Date().getTime();
        }
        var modalInstance = $modal.open({
            templateUrl: templateUrl,
            controller: 'ModalCreateUser',
            size: undefined,
            windowClass: 'modal fade   sizer',
            resolve: {}
        });
        modalInstance.result.then(function(item) {
            item.created_at = $filter('date')(new Date(item.created_at), 'yyyy-MM-dd');
            if(id){
                 UserService.updateUser(item);
            }           
            $scope.items_s = UserService.getUsers();
            for(var i = 0; i < $scope.items_s.length; i++) {
                if($scope.items_s[i].deleted_at== null) {
                    $scope.items_s[i].status = 'yes';
                } else {
                    $scope.items_s[i].status = 'no';
                }
            }
            $scope.tableParams.reload();
        }, function() {});
    };
    
    $scope.resetPassword = function(email){
        UserService.resetPassword(email).then(function(data){
            if(data.status){
                $scope.success_message = data.message;
            }else{
                $scope.error_message = data.message;
            }
        })
    }
}]).controller('ModalChangeStatusCtrl', ['$scope', '$modalInstance', 'UserService','userInfo',
    function($scope, $modalInstance, UserService,userInfo) {
        $scope.userInfo = userInfo;
        $scope.changeStatus = function(){
            UserService.changeStatus($scope.userInfo.id).then(function(data){
                $modalInstance.close(data);
            });
        }
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]).controller('ModalCreateUser', ['$scope', '$timeout', '$modalInstance', 'UserService',
    function($scope, $timeout, $modalInstance, UserService) {
        $scope.formatPhoneNumber = function() {
            angular.element('#phone_number').mask('(999) 999-9999? x99999');
        }

        $scope.validateEmail = true;

        /**
         * update user
         * @return {[type]} [description]
         */
        $scope.createUser = function() {
            UserService.create($scope.userItem).then(function(data) {
                if (data.status == 0) {
                    $scope.error = '';
                    if(typeof data.error == 'undefined'){
                        $scope.error = 'The email has already been taken.';
                    }else{
                        for (var key in data.error) {
                            $scope.error = data.error[key][0];
                        }       
                    }
                } else {
                    $modalInstance.close(data.item);
                }
            });
        };
        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]).directive('uniqueEmail', ["UserService",
    function(UserService) {
        return {
            restrict: 'A',
            controller: 'UserController',
            link: function(scope, el, attrs) {
                //TODO: We need to check that the value is different to the original
                //using push() here to run it as the last parser, after we are sure that other validators were run
                el.blur(function() {
                    var value = el.val();
                    if (value.length == 0) return;
                    UserService.query().then(function(data) {});
                })
            }
        };
    }
]).controller('ModalChangeAvatar', ['$scope', '$timeout', '$modalInstance', '$upload', 'UserService', 'files', 'userId',
function($scope, $timeout, $modalInstance, $upload, UserService, files, userId){
    $scope.myImage = '';
    $scope.myCroppedImage = '';
     var reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = function(evt) {
        $scope.$apply(function() {
            $scope.myImage = evt.target.result;
            $scope.myCroppedImage = evt.target.result;
        });
    };
    $scope.changeAvatar = function() {
        UserService.changeAvatar(userId, $scope.myCroppedImage).then(function(response) {
            $modalInstance.close(response.item.avatar);
        });
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    
}]);

