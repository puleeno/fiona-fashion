var userModule = angular.module('user');
userModule.controller('UserDetailController', ['$scope', '$parse', 'UserService', '$modal', function($scope, $parse, UserService, $modal) {
    $scope.currentItemsRoles = [];
    var checkEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var checkNumber =  /^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/;
    $scope.mobile = function(number){
        $scope.errorMobile = '';
        if(checkNumber.test(number)){
            updateUserChange();
        }else{
            $scope.userProfile.personal_information.work_mobile = "";
            $scope.errorMobile = 'Work Mobile is 10 number';
        }
    }

    $scope.formatPhoneNumber = function() {
        angular.element('#phone_number').mask('(999) 999-9999? x99999');
    }

    $scope.phoneNumber = function(phone) {
        $scope.errorPhone = '';
        if(phone != '') {
            updateUserChange();
        } else {
            $scope.errorPhone = 'Phone is 10 to 15 number';
        }
    }
    var updateUserChange = function() {
        UserService.update($scope.userProfile).then(function(data) {
            $scope.userProfile = data.item;
        });
    }
    $scope.updateUser = function(){
        UserService.update($scope.userProfile).then(function(data){
            $scope.userProfile = data.item;
        });
    }
    $scope.upload = function(files, type) {
        $scope.files = files;
        $scope.getModalCropAvatar();
    }

    $scope.getModalCropAvatar = function(size) {
        var modalInstance = $modal.open({
            templateUrl: window.baseUrl + '/app/components/user/views/modal/myModalContent.html',
            controller: 'ModalChangeAvatar',
            size: size,
            resolve: {
                files: function() {
                    return $scope.files;
                },
                userId: function(){
                     return $scope.userProfile.id;
                }
            },
        });
        modalInstance.result.then(function(avatarUrl) {
             $scope.userProfile.avatar = avatarUrl;
        }, function() {

        });
    };

    $scope.getModalChangePassword = function(id) {
        var modalInstance = $modal.open({
            templateUrl: window.baseUrl + '/app/components/user/views/modal/changePassword.html',
            controller: 'ModalChangePassword',
            size: null,
            resolve: {
                userId: function(){
                    return id;
                },
                isPermission: function(){
                    return $scope.isPermission;
                }
            },
        });
    }

    $scope.getModalCropAvatar = function(size) {
        var modalInstance = $modal.open({
            templateUrl: window.baseUrl + '/app/components/user/views/modal/myModalContent.html',
            controller: 'ModalChangeAvatar',
            size: size,
            resolve: {
                files: function() {
                    return $scope.files;
                },
                userId: function(){
                     return $scope.userProfile.id;
                }
            },
        });
        modalInstance.result.then(function(avatarUrl) {
             $scope.userProfile.avatar = avatarUrl;
        }, function() {

        });
    };
}]);
userModule.controller('ModalChangeAvatar', ['$scope', '$timeout', '$modalInstance', '$upload', 'UserService', 'files', 'userId',
function($scope, $timeout, $modalInstance, $upload, UserService, files, userId){
    $scope.myImage = '';
    $scope.myCroppedImage = '';
     var reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = function(evt) {
        $scope.$apply(function() {
            $scope.myImage = evt.target.result;
            $scope.myCroppedImage = evt.target.result;
        });
    };
    $scope.changeAvatar = function() {
        UserService.changeAvatar(userId, $scope.myCroppedImage).then(function(response) {
            $modalInstance.close(response.item.avatar);
        });
    }
     $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
    
}]);

userModule.controller('ModalChangePassword', ['$scope', '$timeout', '$modalInstance', 'UserService', 'userId', 'isPermission',
function($scope, $timeout, $modalInstance, UserService, userId, isPermission){
    $scope.isPermission = isPermission;
    $scope.changePassword = function() {
        $scope.user['id'] = userId;
        UserService.changePassword($scope.user).then(function(data) {
            $scope.error = null;
            $scope.message_success = 'Change password success';
        }, function(r) {
            $scope.error = r.error;
        });
    }

    $scope.close = function () {
        $modalInstance.dismiss('cancel');
  };
    
}]);


