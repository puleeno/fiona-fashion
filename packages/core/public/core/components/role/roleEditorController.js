var roleModule = angular.module('role');

roleModule.controller('RoleController', ['$scope', '$parse', 'RoleService', '$modal','$filter', function($scope, $parse, RoleService, $modal, $filter) {
      
      $scope.baseUrl = window.baseUrl;
      $scope.items_s = RoleService.setData(angular.copy(window.dataRoles));
	  $scope.permissionMap = RoleService.getPermissionMap();
      
      /**
       * delete permission of rle
       * @return {[type]} [description]
       */
	  $scope.deletePermission = function(permissionId)
	  {
	  	if (!confirm('Do you want delete this permission')) return;
	  	 RoleService.deletePermission($scope.role.id, permissionId).then(function(data){
	  	 	 $scope.items_s = RoleService.getData();

	  	 	 console.log( $scope.items_s,'dsfdsf');
	  	 });
	  }

	  $scope.availablePermission = function(permissionId) {
	  	RoleService.updatePermissions($scope.role.id, permissionId).then(function(data){
	  		if(data.status) {
	  			for (var key in $scope.items_s.permissionList) {
	  				if($scope.items_s.permissionList[key].id == permissionId) {
	  					$scope.items_s.items.perms.push($scope.items_s.permissionList[key]);
	  					$scope.items_s.permissionList.splice(key, 1);
	  					break;
	  				}
	  			}
	  		}		
	  	});
	  }


	  $scope.updateRole = function (role) {
	  	RoleService.update(role).then(function(data){
          console.log(data,'dsfdsf');
	  	});
	  }


}]);

