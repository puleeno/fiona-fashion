var roleModule = angular.module('role');

roleModule.controller('RoleController', ['$scope', '$parse', 'RoleService', '$modal','$filter', function($scope, $parse, RoleService, $modal, $filter) {
	$scope.baseUrl = window.baseUrl;
    $scope.currentRoleIdActive = 0;
    $scope.role = {};
	$scope.items_s = RoleService.setData(angular.copy(window.dataRoles));
	$scope.permissionMap = RoleService.getPermissionMap();
	if(typeof window.dataController != 'undefined'){
		for(var key in window.dataController){
			var model = $parse(key);
			model.assign($scope, window.dataController[key]);
		}
	}
	$scope.moveRole = function (value) {
        if ($scope.items_s.items.indexOf(value) != -1) {
            $scope.isActive = {};
            $scope.isActive[value.id] = true;
            $scope.currentRoleIdActive = value.id;
        }
    }

	$scope.updatePermission = function(permissionId){
    if ($scope.currentRoleIdActive == 0) {
        alert('Role must be active before choosing');
    } else {
        RoleService.updatePermissions($scope.currentRoleIdActive, permissionId).then(function(data){
            if (data.status == true) {
                var element = angular.element('#box-assignee .w-w-item.active').attr('id');
                var key1 = parseInt(element.split('role-user-')[1]);
                if ($scope.items_s.items[key1].permissions.length == 0) {
                    $scope.items_s.items[key1].permissions.push(permissionId);
                } else {
                    for (var key in $scope.items_s.items[key1].permissions) {
                        if ($scope.items_s.items[key1].permissions.indexOf(permissionId) == -1) {
                            $scope.items_s.items[key1].permissions.push(permissionId);
                        }
                    }
                }
                
                $scope.isPerActive = {};
                
                $scope.isPerRoleActive = {};
             
                if(angular.isUndefined($scope.isPerActive[$scope.currentRoleIdActive])) {
                  $scope.isPerActive[$scope.currentRoleIdActive] = {};
                }

                $('.role-id-' + $scope.currentRoleIdActive).css('display','block');
                $scope.isPerRoleActive[$scope.currentRoleIdActive] = true;
                $scope.isPerActive[$scope.currentRoleIdActive][permissionId] = true;
            }
        });
    }
		
	}


    $scope.setRoleIdActive = function (roleId, event) {
        $scope.isPerRoleActive = {};
        $scope.isPerRoleActive[roleId] = true;
        $scope.isActive = {};
        $scope.isActive[roleId] = true;
        $scope.currentRoleIdActive = roleId;
    }

    $scope.delete = function(id) {
        if (!confirm('Do you want delete this role')) return;
        RoleService.remove(id).then(function(data) {
            $scope.items_s = RoleService.getData();
            $scope.currentRoleIdActive = 0;
        });
    }

    $scope.getModalCreateRole = function(id) {
        var templateUrl = 'admin/user/roles/create';
        if (typeof id != 'undefined') {
            templateUrl = 'admin/user/roles/' + id + '/edit' + '?' + new Date().getTime();
        }
        var modalInstance = $modal.open({
            templateUrl: templateUrl,
            controller: 'ModalCreateRole',
            size: undefined,
            resolve: {}
        });
        modalInstance.result.then(function(selectedItem) {
            $scope.items_s = RoleService.getData();
        }, function() {});
    };

    $scope.showGroup = function($event){
        var w = $(window).outerWidth();
        $($event.target).parent().toggleClass("ac-up");
        $('.group-btn-ac').css({
            top: $event.pageY  + 'px',
            right: w - $event.pageX - 30 + 'px',
        });
        $(document).on('click', function closeMenu (e){
            $(e.target).closest('tr').siblings().find('.wrap-ac-group').removeClass('ac-up');
            if ($('.wrap-ac-group').has(e.target).length === 0){
                $('.wrap-ac-group').removeClass('ac-up');
            } else {
                $(document).one('click', closeMenu);
            }
        });
        angular.element('.table-responsive').addClass('fix-height');
    };

    $scope.showPermisionOfRoleId = function(id) {
        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: window.baseUrl+'/admin/user/roles/permissions/'+id+ '?' + new Date().getTime(),
          controller: 'ShowRolePermissionCtrl',
          resolve: {
          }
        });

        modalInstance.result.then(function (selectedItem) {
        }, function () {
        });
    }
}]);

roleModule.controller('ShowRolePermissionCtrl', ['$scope', '$modalInstance',  function ($scope, $modalInstance) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

roleModule.controller('ModalCreateRole', ['$scope', '$timeout', '$modalInstance', 'RoleService',function($scope, $timeout, $modalInstance, RoleService) {
    $timeout(function() {
        angular.element("#permission-select").select2();
        if (typeof $scope.role != 'undefined') angular.element("#permission-select").select2('val', $scope.role.permissions);
    }, 100)

    $scope.createRole = function() {
        angular.element("#bt-submit").attr("disabled", "true");
        $scope.role.name = angular.lowercase($scope.role.name).trim().replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,"_").replace(/\s+/g,"_");
        RoleService.create($scope.role).then(function(data) {
            if (data.status == 0) {
                angular.element("#bt-submit").removeAttr("disabled");
                $scope.error = '';
                for (var key in data.error) {
                    $scope.error = data.error[key][0];
                }
            } else {
                $modalInstance.close(data.item);
            }
        });
    };
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
}]);
