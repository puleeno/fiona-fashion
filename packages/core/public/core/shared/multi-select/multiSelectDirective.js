var module = angular.module('multiSelect', []);
module.directive('multiSelect', [function(){
	return {
		restrict: 'EA',
		scope: {
			items : '=',
			itemsAssigned : '=',
			placeholder:'@',
			onChange: '&'
		},
		replace: true,
		templateUrl: '/app/shared/multi-select/view.html?v=2',
		link: function($scope, $elem, $attr){
			$scope.assignPermissions = function(){
				if($scope.selectedItemFrom.length == 0) return;
				console.log('from', $scope.selectedItemFrom);
				for(var key in $scope.selectedItemFrom){
					var value = $scope.selectedItemFrom[key];
					$scope.itemsAssigned[value] = $scope.items[value];
					delete $scope.items[value];
				}
				$scope.selectedItemFrom = [];
				console.log('tttt', $scope.itemsAssigned);
				$scope.onChange();
			}

			$scope.undoPermissions = function(){
				if(angular.isArray($scope.items)){
					$scope.items = {};
				}
				
				if($scope.selectedItemTo.length == 0) return;
				console.log('to', $scope.selectedItemTo);

				for(var key in $scope.selectedItemTo){
					var value = $scope.selectedItemTo[key];
					$scope.items[value] = $scope.itemsAssigned[value];
					delete $scope.itemsAssigned[value];
				}
				$scope.selectedItemTo = [];
				console.log('tttt', $scope.itemsAssigned);
				$scope.onChange();
			}
		}
	};
}])