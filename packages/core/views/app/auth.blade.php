
<!DOCTYPE Html>

<html id="login" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Comus</title>
        <base href="{!!URL::to('/')!!}">
        @include('shared.head')
        <script type="text/javascript">
            var baseUrl = '{!!URL::to('/')!!}';
        </script>
    </head>

    <body id="login_body" class="wrap-auth">
        <div class="content-auth">      
        @yield('content')
        </div> 
        <a href="#" title="Explore Mortgage Branch Opportunities Across the Country" id="outbound">
            Mortgage Branch Opportunities
        </a>

    </body>

</html>