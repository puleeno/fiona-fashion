@extends('app')
@section('content')
<div data-ng-controller="PermissionController" ng-init="perm={{json_encode($infoPermission)}}">
   <ol class="breadcrumb breadcrumb-permission"> 
    <li><a href="{{URL::to('/')}}/admin/user/permissions">Manage Permissions</a></li> 
    <li>Permission Editor</li> 
</ol>
<div class="wrap-permission-user fix-top-w-edit">
    <div class="col-lg-12 w-r-edit">
        <div class="form-group">
            <label class="col-sm-4 col-md-3 col-lg-2 label-title-edit">Permission Name :</label>
            <div class="col-sm-8 col-md-9 col-lg-10">
                <div class="fix-input-pl">
                    <a href="#" onaftersave="updatePerm(perm)" e-required="true" editable-text="perm.display_name">@{{ perm.display_name || 'empty' }}</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 col-md-3 col-lg-2  label-title-edit">Permission Slug : </label>
            <div class="col-sm-8 col-md-9 col-lg-10">
                <a href="javascript:void(0)">@{{perm.name}}</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label class="col-lg-12 form-group">Permission Decription : </label>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fix-btn-pl">
                <a href="#" onaftersave="updatePerm(perm)" e-required="true" editable-textarea="perm.description" e-class="form-control" e-rows="5" e-cols="197">
                    <pre>@{{ perm.description || 'no description' }}</pre>
                </a>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <hr>
        <div class="col-lg-12 box-w-n">
            <div class="table-responsive table-action-user" ng-controller="PerUserController"> 
                <table class="table" style="min-width:100px;">
                        <thead>
                            <th>@{{us ? 'User': 'Group'}}</th>
                            <th class="text-right action-add-u">
                                <a ng-class="{'active':us}" ng-click="clickUser()" class=""><i class="fa fa-user"></i><i class="fa fa-plus"></i></a>
                                <a ng-class="{'active': gr}" ng-click="clickGroup()" class=""><i class="fa fa-group"></i><i class="fa fa-plus"></i></a>
                            </th>
                        </thead>
                        <tbody>
                            <tr ng-if="us">
                                <td colspan="3">
                                <search type="false" items="users" ng-model="permission.user_id" user-id="{{Auth::user()->id}}" placeholder="Select User" on-change="changUser()"></search>
                                </td>
                            </tr>
                            <tr ng-if="gr">
                                <td colspan="3">
                                <select class="form-control" ng-model="permission_group.group_id" ng-options="item.id as item.name for item in groups" ng-change="changeGroup(item)">
                                  <option value="">Select Group</option>
                                </select>
                                </td>
                            </tr>
                            <tr ng-repeat="item in users_permission">
                                <td>
                                    <i class="fa fa-user"></i>
                                    <a href="" style="margin-left:50px;">@{{item.first_name}} @{{item.last_name}}</a>
                                </td>
                                <td>
                                    <a  ng-click="delete(item)"><i class="fa fa-trash pull-right"></i></a>
                                </td>
                            </tr>
                            <tr ng-repeat="item in groups_permission">
                                <td>
                                    <i class="fa fa-group"></i>
                                    <a href="" style="margin-left:50px;">@{{item.name}}</a>
                                </td>
                                <td>
                                    <a ng-click="deleteGroup(item)"><i class="fa fa-trash pull-right"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div> 
</div>
@stop
@section('scripts-modules')
	<script>
        window.dataPerms = {
            items: {!! json_encode($infoPermission) !!}
        };
        window.users_permission = {!!json_encode($users_permission)!!};
        window.groups = {!!json_encode($groups)!!};
        window.groups_permission = {!!json_encode($groups_permission)!!};
        window.permission_id = {!!json_encode($id)!!};
	    modules = ['xeditable','ngTable'];
	</script>
@stop
@section('scripts')
	 @if(!isProduction() && !isDev())
		{!! Html::script('app/components/permission/permissionController.js?v=getVersionScript()')!!}
        {!! Html::script('app/components/permission/permissionService.js?v=getVersionScript()')!!}
        {!! Html::script('app/components/permission/permissionDirective.js?v=getVersionScript()')!!}

        {!! Html::script('app/components/permission/peruserController.js?v=getVersionScript()')!!}
        {!! Html::script('app/components/permission/peruserService.js?v=getVersionScript()')!!}
        {!! Html::script('app/shared/search/searchDirective.js')!!}
	@else
	    <script src="{{ elixir('app/pages/permissionEditor.js') }}"></script>
	@endif
@stop