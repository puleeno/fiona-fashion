@extends('app')
@section('title') Permissions | @stop
@section('content')
<div class="wrap-branch" data-ng-controller="PermissionController">
	<permissions items="{{$items}}"></permissions>
</div>
@stop
@section('scripts-modules')
	<script>
	    modules = ['xeditable','ngTable'];
	</script>
@stop
@section('scripts')
	 @if(!isProduction() && !isDev())
		{!! Html::script('app/components/permission/permissionController.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/permission/permissionService.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/permission/permissionDirective.js?v=getVersionScript()')!!}
	@else
	    {{-- {!! Html::script('app/pages/permission.js?v='.getVersionScript())!!} --}}
	    <script src="{{ elixir('app/pages/permission.js') }}"></script>
	@endif
@stop