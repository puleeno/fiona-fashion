<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
	<h4 class="modal-title" ng-bind="title_permission"></h4>
</div>
<form class="form-horizontal" method="POST" action="{{{ URL::to('roles') }}}" accept-charset="UTF-8" name="formAddPermission" ng-init="permission={{json_encode($infoPermission)}}">
	<div class="modal-body">	
		<input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
		<fieldset>
		<div class="form-group" ng-class="{'has-error':formAddPermission.name.$touched && formAddPermission.name.$invalid}">
			<label for="name" >Name <small>*</small></label>
			<div >
				<input class="form-control" placeholder="Name" type="text" name="name" id="name"  ng-model="permission.name" ng-required="true" ng-if="!permission.id">
				<label ng-if="permission.id" style="margin-top:5px"><h3>@{{permission.name}}</h3></label>
				<label class="control-label" ng-show="formAddPermission.name.$touched && formAddPermission.name.$invalid" >
					Name invalid
				</label>
			</div>
		</div>


		<div class="form-group" ng-class="{'has-error':formAddPermission.display_name.$touched && formAddPermission.display_name.$invalid}">
			<label for="display_name" >Display Name <small>*</small></label>
			<div >
				<input class="form-control" placeholder="Display Name" type="text" name="display_name" id="display_name"  ng-model="permission.display_name" ng-required="true">
				<label class="control-label" ng-show="formAddPermission.display_name.$touched && formAddPermission.display_name.$invalid" >
					Display Name invalid
				</label>
			</div>
		</div>

		<div class="form-group" ng-class="{'has-error':formAddPermission.description.$touched && formAddPermission.description.$invalid}">
			<label for="description" >Description <small>*</small></label>
			<div >
				<textarea class="form-control" rows="5" placeholder="Description" type="text" name="description" id="description"  ng-model="permission.description" ng-required="true">
				</textarea>
				<label class="control-label" ng-show="formAddPermission.description.$touched && formAddPermission.description.$invalid" >
					Description invalid
				</label>
			</div>
		</div>

		<div class="alert alert-danger" ng-show="error">
			@{{error}}
		</div>
			
		</fieldset>
	</div>
</form>
<div class="modal-footer center-block">
	<button id="bt-submit" type="submit" class="btn btn-primary" ng-disabled="formAddPermission.$invalid" ng-click="createPermission()" ng-if="!permission.id"><i class="fa fa-check"></i> Save</button>
	<button id="bt-submit" class="btn btn-primary" ng-disabled="formAddPermission.$invalid" ng-click="editPermission(permission.id)"  ng-if="permission.id"><i class="fa fa-check"></i> Save</button>
	<button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Cancel</button>
</div>