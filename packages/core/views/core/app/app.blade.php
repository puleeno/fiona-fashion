<!DOCTYPE html>
<html class="st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l2" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Comus</title>
    <link href="{{ asset('/css/theme-bundle.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
</head>
<body>
    <!-- Wrapper required for sidebar transitions -->
    <div class="st-container">
        <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="#sidebar-menu" data-toggle="sidebar-menu" data-effect="st-effect-3" class="toggle pull-left visible-xs"><i class="fa fa-bars"></i></a>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand hidden-xs navbar-brand-primary">LOGO</a>
                </div>
                <div class="navbar-collapse collapse" id="collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
                            <li><a href="{{ url('/auth/login') }}">Login</a></li>
                            <li><a href="{{ url('/auth/register') }}">Register</a></li>
                        @else
                            <li><a>{{ Auth::user()->name }}</a></li>
                            <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <!-- content push wrapper -->
        <div class="st-pusher">
            <!-- Sidebar component with st-effect-3 (set on the toggle button within the navbar) -->
            <div class="sidebar left sidebar-size-2 sidebar-offset-0 sidebar-skin-blue sidebar-visible-desktop" id="sidebar-menu" data-type="collapse">
                <div class="split-vertical">
                    
                    <div class="split-vertical-body">
                        <div class="split-vertical-cell">
                            <div class="tab-content">
                                <div class="tab-pane active" id="sidebar-tabs-menu">
                                    <div data-scrollable>
                                        <ul class="sidebar-menu sm-icons-right sm-icons-block">
                                            <li class="active"><a href="/"><i class="fa fa-home"></i> <span>Home</span></a>
                                            </li>
                                            @if(\Auth::check())
                                                @if(\Auth::user()->is('super.admin') || \Auth::user()->can('user.admin'))
                                                    <li>
                                                        <a href="/admin/user"><i class="fa fa-user"></i> <span>User</span></a>
                                                    </li>

                                                @endif
                                                @if(\Auth::user()->is('super.admin'))
                                                <li><a href="/admin/user/roles"><i class="fa fa-puzzle-piece"></i> <span>Roles</span></a>
                                                </li>
                                                <li><a href="/admin/user/permissions"><i class="fa fa-magic"></i> <span>Permissions</span></a>
                                                </li>
                                                @endif
                                            @endif
                                            
                                        </ul>
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- // END .tab-content -->
                        </div>
                        <!-- // END .split-vertical-cell -->
                    </div>
                    <!-- // END .split-vertical-body -->
                </div>
            </div>
            <!-- this is the wrapper for the content -->
            <div class="st-content" id="content">
                <div class="st-content-inner">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
            </div>
            <!-- /st-content -->
        </div>
    </div>
    <!-- Scripts -->
    @yield('script')
</body>
</html>