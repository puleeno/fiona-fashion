<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Tìm kiếm...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>

            <li>
                <a href="/admin/dashboard"><i class="fa fa-dashboard fa-fw"></i> Control Panel</a>
            </li>

            @if(Auth::check() && (Auth::user()->is('super.admin') || Auth::user()->is('super.mod')))
                <li @if(Request::is('admin/category') || Request::is('admin/category/*')) class="active" @endif>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Categories<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/category" class="active">Categories List</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
           	@endif

           	@if(Auth::user()->is('super.admin'))
                <li @if(Request::is('admin/user')) class="active" @endif>
                    <a href="#"><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/admin/user" class="active">Users List</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            @endif
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>