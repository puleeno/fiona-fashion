<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
@if(!empty($item->id))
<h4 class="modal-title">Edit User @{{$item.first_name}} @{{$item.last_name}}</h4>
@else
<h4 class="modal-title">Create User</h4>
@endif
	
</div>
<div class="modal-body user-modal">
	<form  class="" method="POST" accept-charset="UTF-8" name="formAddUser"  ng-init='userItem={{$item}}'>
		<div class="form-group" ng-class="{'has-error':formAddUser.first_name.$touched && formAddUser.first_name.$invalid}">
			<label for="first_name">First Name<small>*</small></label>
			<input class="form-control"  placeholder="First name" type="text" name="first_name" id="first_name" value="" ng-model="userItem.first_name" ng-required="true">
			<label class="control-label" ng-show="formAddUser.first_name.$touched && formAddUser.first_name.$invalid">
				First Name invalid
			</label>
		</div>

		<div class="form-group" ng-class="{'has-error':formAddUser.last_name.$touched && formAddUser.last_name.$invalid}">
			<label for="last_name">Last Name<small>*</small></label>
			
			<input class="form-control"  placeholder="Last Name" type="text" name="last_name" id="last_name" value="" ng-model="userItem.last_name" ng-required="true">
			<label class="control-label" ng-show="formAddUser.last_name.$touched && formAddUser.last_name.$invalid">
				Last Name invalid
			</label>
		</div>

		<div class="form-group" ng-class="{'has-error':formAddUser.email.$touched && formAddUser.email.$invalid}">
			<label for="email">Email <small>*</small></label>
			
			<input ng-pattern="/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/" class="form-control" placeholder="Email" type="text" name="email" id="email"  ng-model="userItem.email" ng-required="true">
			<label class="control-label" ng-show="formAddUser.email.$touched && formAddUser.email.$invalid" >
				Email invalid
			</label>
		</div>

		<div class="form-group" ng-class="{'has-error':formAddUser.phone_number.$touched && formAddUser.phone_number.$invalid}">
			<label for="phone_number">Phone</label>
			<div class="">
				<input class="form-control" type="text" name="phone_number" id="phone_number"
						ng-model="userItem.personal_information.phone_number"
						data-ng-init='formatPhoneNumber()' ng-blur="validatePhone(userItem.personal_information.phone_number)">
				<label  class="control-label" ng-show="formAddUser.phone_number.$touched && formAddUser.phone_number.$invalid">
					Not a phone number!
				</label>
			</div>
		</div>
	</form>
	<div class="alert alert-error alert-danger" ng-show="error">
		@{{error}}
	</div>
	<div class="alert" ng-show="notice">@{{notice}}</div>
</div>
<div class="modal-footer">
	<div class="form-group center-block">
		<button ng-disabled="formAddUser.$invalid" class="btn btn-action" ng-click="createUser()">Add</button>
		<button class="btn btn-default" ng-click="cancel()">Cancel</button>
	</div>
</div>
