@extends(getapp())
@section('title')
    Users
@stop
@include('users::shared.head')
@section('content')
    <div class="top-content">
        <ul class="breadcrumb br-ticket1 top-content">
            @if(Auth::user()->is('super.admin') || Auth::user()->can('user.admin'))
            <li>
                <a href="{{ URL::to('admin/user')}}">Manage Users</a>
            </li>
            @endif
            <li class="active">
                <a  href="javascript:void(0)">Edit Profile For <strong>{{$item['first_name']}} {{$item['last_name']}}</strong></a>
            </li>
        </ul>
    </div>
    <div class="content content-profile" ng-app="users-rowboat" id="module_profile" ng-controller="UserDetailController">
        <div class="user-profile-info" ng-init="userProfile={{json_encode($item)}}; isPermission={{(Auth::user()->is('super.admin')) || (Auth::user()->can('user.admin')) || (Auth::user())}}">
            <div class="col-fix col-md-2 col-xs-4">
                <div class="">
                    <img ng-src="@{{userProfile.avatar}}" style="width:100%;">
                    <a class="camera" type="submit" data-toggle="modal" data-target="#change_avatar" class="fa fa-camera edit-image" ng-model="file"  accept="image/*" ng-file-select ng-file-drop ng-file-change="upload($files)"><i class="fa fa-camera"></i></a>
                </div>
                <div class="btn-change">
                    <button class="btn btn-primary col-lg-12" data-toggle="modal" ng-click="getModalChangePassword(userProfile.id)">
                        Change Passwords
                    </button>
                    <fiv class="clearfix"></fiv>
                </div>
            </div>
            <div class="col-fix personal-info col-md-5 col-xs-8">
                <div>
                    <dl class="dl-horizontal">
                        <h3>Personal Infomation</h3>
                        <p class="first">
                            <strong>First name: </strong>
                            <span> 
                                <a href="#" editable-text="userProfile.first_name" e-name="userProfile.first_name" onaftersave="updateUser(userProfile.first_name)">@{{userProfile.first_name || 'empty'}} 
                                </a>
                            </span>
                            <p>
                                <strong>Last name: </strong>
                                <span> 
                                    <a href="#" editable-text="userProfile.last_name" e-name="userProfile.last_name" onaftersave="updateUser(userProfile.last_name)">@{{userProfile.last_name || 'empty'}} 
                                    </a>
                                </span>
                            </p>
                        </p>                
                    </dl>
                </div>
            </div>
            <div class="group-info col-md-5 col-xs-12 col-sm-12">
                <div class="other-information">
                    <h3>Other Information</h3>
                    <p class="first">
                        <strong>Phone Number: </strong>
                        <span> 
                            <a  editable-text="userProfile.personal_information.phone_number"
                                e-name="userProfile.personal_information.phone_number"
                                e-id="phone_number"
                                e-data-ng-init="formatPhoneNumber()"
                                onaftersave="phoneNumber(userProfile.personal_information.phone_number)">
                                @{{(userProfile.personal_information.phone_number || 'empty')}} 
                            </a>
                        </span>&nbsp&nbsp&nbsp
                        <p><strong>Email: </strong>
                            <span> 
                                <a editable-text="userProfile.email" e-name="userProfile.email" onaftersave="updateUser(userProfile.email)" >@{{userProfile.email || 'empty'}} </a>
                            </span>
                        </p>
                        <span class="has-error" ng-if="userProfile.personal == ''">The email has already been taken.</span>
                    </p>   
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    @include('users::shared.script')
@stop
@section('script')
    {!! Html::script('app/components/user/userService.js')!!}
    {!! Html::script('app/components/user/profileUserController.js')!!}
    <script type="text/javascript">
        window.baseUrl = '{{URL::to("")}}';
        jQuery.noConflict();
        jQuery( document ).ready(function( $ ) {
            // replace image when changing image
            $('#profile_change_picture').change(function(){
                var oFReader = new FileReader();
                oFReader.readAsDataURL(this.files[0]);
                console.log(this.files[0]);
                oFReader.onload = function (oFREvent) {
                    $('#current_profile_img').attr("src", oFREvent.target.result);
                };
            });
        });
    </script>
@stop

