@extends('app')
@section('content')
<div data-ng-controller="RoleController" ng-init="role={{json_encode($item)}}">
   <ol class="breadcrumb breadcrumb-permission"> 
    <li><a href="{{URL::to('/')}}/admin/user/roles">Manage Roles</a></li> 
    <li>Role Editor</li> 
</ol>
<div class="wrap-permission-user fix-top-w-edit">
    <div class="col-lg-12 w-r-edit">
        <div class="form-group">
            <label class="col-sm-4 col-md-3 col-lg-2 label-title-edit">Role Name :</label>
            <div class="col-sm-8 col-md-9 col-lg-10">
                <div class="fix-input-pl">
                    <a href="#" onaftersave="updateRole(role)" e-required="true" editable-text="role.display_name">@{{ role.name || 'empty' }}</a>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 col-md-3 col-lg-2 label-title-edit">Role Slug : </label>
            <div class="col-sm-8 col-md-9 col-lg-10">
                <span>@{{role.slug}}</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="form-group">
            <label class="col-lg-12 form-group">Role Description : </label>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 fix-btn-pl">
                <a href="#" onaftersave="updateRole(role)" e-required="true" editable-textarea="role.description" e-class="form-control" e-rows="5" e-cols="197">
                    <pre>@{{ role.description || 'no description' }}</pre>
                </a>
            </div>
            
            <div class="clearfix"></div>
        </div>
        <hr>
        <div class="col-lg-6 box-w-m">
            <div class="box-role">
                <h5 class="margin-none title-box-r">Available Permissions</h5>
                <div id="box-role-fix">
                    <div class="w-item" ng-click="availablePermission(value.id)" ng-repeat="(key, value) in items_s.permissionList | orderBy:'display_name'">
                        <span class="item pull-left">@{{value.display_name}}</span>
                        <span class="action pull-right">
                        <i class="fa fa-arrow-circle-o-right"></i>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 box-w-m">
            <div class="box-role">
                <h5 class="margin-none title-box-r">Assigned Permissions</h5>
                <div  id="box-assignee-fix">
                    <div class="w-item" ng-repeat="(key, value) in items_s.items.perms | orderBy:'display_name'">
                        <span class="item toggle-role pull-left">@{{value.display_name}}</span>
                        <span class="action pull-right">
                        <i class="fa fa-trash" ng-click="deletePermission(value.id)"></i>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 box-w-n">
            <div class="table-responsive table-action-user" ng-controller="RoleUserController"> 
                <table class="table" style="min-width:100px;">
                        <thead>
                            <th>@{{us ? 'User': 'Group'}}</th>
                            <th class="text-right action-add-u">
                                <a ng-class="{'active':us}" ng-click="clickUser()" class=""><i class="fa fa-user"></i><i class="fa fa-plus"></i></a>
                                <a ng-class="{'active': gr}" ng-click="clickGroup()" class=""><i class="fa fa-group"></i><i class="fa fa-plus"></i></a>
                            </th>
                        </thead>
                        <tbody>
                            <tr ng-if="us">
                                <td colspan="3">
                                <search type="false" items="users" ng-model="role.user_id" user-id="{{Auth::user()->id}}" placeholder="Select User" on-change="changUser()"></search>
                                </td>
                            </tr>
                            <tr ng-if="gr">
                                <td colspan="3">
                                <select class="form-control" ng-model="role_group.group_id" ng-options="item.id as item.name for item in groups" ng-change="changeGroup(item)">
                                  <option value="">Select Group</option>
                                </select>
                                </td>
                            </tr>
                            <tr ng-repeat="item in users_role" ng-show="us">
                                <td>
                                    <i class="fa fa-user"></i>
                                    <a href="" style="margin-left:50px;">@{{item.first_name}} @{{item.last_name}}</a>
                                </td>
                                <td>
                                    <a  ng-click="delete(item)"><i class="fa fa-trash pull-right"></i></a>
                                </td>
                            </tr>
                            <tr ng-repeat="item in groups_role" ng-show="gr">
                                <td>
                                    <i class="fa fa-group"></i>
                                    <a href="" style="margin-left:50px;">@{{item.name}}</a>
                                </td>
                                <td>
                                    <a ng-click="deleteGroup(item)"><i class="fa fa-trash pull-right"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div> 
</div>
@stop
@section('scripts-modules')
	<script>
        window.dataRoles = {
            items: {!! json_encode($item) !!},
            permissionList: {!! json_encode($permissionList) !!}
        };
        window.users_role = {!!json_encode($users_role)!!};
        window.groups = {!!json_encode($groups)!!};
        window.groups_role = {!!json_encode($groups_role)!!};
        window.role_id = {!!json_encode($id)!!};
	    modules = ['xeditable','ngTable'];
	</script>
@stop
@section('script')
	 @if(!isProduction() && !isDev())
		{!! Html::script('app/components/role/roleEditorController.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/role/roleService.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/role/roleDirective.js?v=getVersionScript()')!!}

        {!! Html::script('app/components/role/roleuserController.js?v=getVersionScript()')!!}
        {!! Html::script('app/shared/search/searchDirective.js')!!}
        {!! Html::script('app/components/role/roleuserService.js?v=getVersionScript()')!!}
	@else
	    <script src="{{ elixir('app/pages/roleEditor.js') }}"></script>
	@endif
@stop