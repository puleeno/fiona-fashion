<div class="modal-header">
<button aria-label="Close" data-dismiss="modal" class="close" type="button" ng-click="cancel()"><span aria-hidden="true">×</span></button>
	@if(!empty($item->id))
	<h4 class="modal-title">Edit Role {{$item->display_name}}</h4>
	@else
	<h4 class="modal-title">Create Role</h4>
	@endif
	
</div>
<div class="modal-body">

	<form class="form-horizontal" method="POST" action="{{{ URL::to('roles') }}}" accept-charset="UTF-8" name="formAddRole" ng-init="role={{$item}}">
		<input type="hidden" name="_token" value="{{{ Session::getToken() }}}">
		<fieldset>
			<div class="form-group" ng-class="{'has-error':formAddRole.name.$touched && formAddRole.name.$invalid}">
				<label for="name">Name <small>*</small></label>
				<div>
					<input ng-if="!role.id" class="form-control" placeholder="Name" type="text" name="name" id="name"  ng-model="role.name" ng-required="true">
					<label ng-if="role.id" style="margin-top:5px"><h3>@{{role.name}}</h3></label>
					<label class="control-label" ng-show="formAddRole.name.$touched && formAddRole.name.$invalid" >
						Name invalid
					</label>
				</div>
			</div>
			
			<div class="form-group" ng-class="{'has-error':formAddRole.display_name.$touched && formAddRole.display_name.$invalid}">
				<label for="display_name">Display Name <small>*</small></label>
				<div>
					<input class="form-control" placeholder="Display Name" type="text" name="display_name" id="display_name"  ng-model="role.display_name" ng-required="true">
					<label class="control-label" ng-show="formAddRole.display_name.$touched && formAddRole.display_name.$invalid" >
						Display Name invalid
					</label>
				</div>
			</div>

			<div class="form-group" ng-class="{'has-error':formAddRole.description.$touched && formAddRole.description.$invalid}">
				<label for="description">Description <small>*</small></label>
				<div>
					<textarea class="form-control" rows="5" placeholder="Description" type="text" name="description" id="description"  ng-model="role.description" ng-required="true">
					</textarea>
					<label class="control-label" ng-show="formAddRole.description.$touched && formAddRole.description.$invalid" >
						Description invalid
					</label>
				</div>
			</div>

			<div class="alert alert-danger" ng-show="error">
				@{{error}}
			</div>
		</fieldset>	

	</form>
</div>
<div class="modal-footer center-block">
	<button id="bt-submit" class="btn btn-primary" ng-disabled="formAddRole.$invalid" ng-click="createRole()"><i class="fa fa-check"></i> Save</button>
	<button class="btn btn-default" ng-click="cancel()"><i class="fa fa-times"></i> Cancel</button>
</div>