<div class="modal-header">
	<button type="button" class="close"  ng-click="cancel()" aria-hidden="true">×</button>
	<h3 class="modal-title">Roles</h3>
</div>
<!-- // Modal heading END -->

<!-- Modal body -->
<div class="modal-body" ng-init="rolePermissions={{json_encode($rolePermissions)}}">
	<div class="w-w-item"> 
		<div class="w-item"> 
			<span class="item" style="font-weight:700">{{$role->display_name}}</span> 
		</div> 
		<div class="item-ch" ng-repeat="(key, value) in rolePermissions"> 
			<span>-</span> <span>@{{value.display_name}}</span> 
		</div> 
	</div>
</div>
<!-- // Modal body END -->

<!-- Modal footer -->
<div class="modal-footer">
	<a href="javascript:void(0)" ng-click="cancel()" class="btn btn-default" data-dismiss="modal">Close</a> 
</div>
