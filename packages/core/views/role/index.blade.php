@extends('app')
@section('title') Roles | @stop
@section('content')
<div data-ng-controller="RoleController">
<ol class="breadcrumb breadcrumb-permission fix-br-role"> 
   <h4 class="margin-none">Roles Manager</h4>
</ol>
<a href="javascript:void(0)" ng-click="getModalCreateRole()" class="btn btn-primary pull-right btn-add-role-fix"><i class="fa fa-plus"></i> Add Role</a>
<div class="wrap-permission-user fix-bottom-m">
    <div class="col-lg-5 box-w-m">
        <div class="box-role">
            <h5 class="margin-none title-box-r">Roles</h5>
            <div id="box-role">
                <div class="w-item"  ng-repeat="(key, value) in items_s.items">
                    <span class="item pull-left">@{{value.display_name}}</span>
                    <span class="action pull-right">
                    <i class="fa fa-list" ng-click="showPermisionOfRoleId(value.id)"></i>
                    <a href="@{{baseUrl}}/admin/user/roles/@{{value.id}}/edit"><i class="fa fa-edit"></i></a>
                    <i class="fa fa-arrow-circle-o-right" ng-click="moveRole(value)"></i>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="box-permission">
            <h5 class="margin-none title-box-r">Permissions</h5>
            <div  id="box-permission">
                <div class="w-item" ng-click="updatePermission(value.id)" ng-repeat="(key, value) in items_s.permissionList">
                    <span class="item pull-left">@{{value.name}}</span>
                    <span class="action pull-right">
                        <i class="fa fa-arrow-circle-o-right"></i>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-7 box-w-m" >
        <div class="box-role" id="box-assignee">
            {{-- <h5 class="margin-none title-box-r">Assinees</h5> --}}
            <div class="w-w-item toggle-role-p" id="role-user-@{{key}}" ng-class="{'active': isActive[value.id]}" ng-click="setRoleIdActive(value.id, $event)" ng-repeat="(key, value) in items_s.items">
                <div class="w-item">
                    <span class="item toggle-role pull-left"><i class="fa fa-plus" ng-class="{'fa-pluss': isPerRoleActive[value.id]}" style="font-size:15px;color:#649bd7"> </i> @{{value.display_name}}</span>
                    <span class="action pull-right">
                    <i class="fa fa-trash" ng-click="delete(value.id)"></i>
                    </span>
                    <div class="clearfix"></div>
                </div>


                <div id="w-item-ch" style="display: none;" class="w-item-role-per role-id-@{{value.id}}">

                    <div class="item-ch" ng-class="{'active':isPerActive[value.id][value1]}" ng-repeat="(key1, value1) in value.permissions">


                        <span>-</span> <span>@{{permissionMap[value1].display_name}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</div>

@stop
@section('scripts-modules')
	<script>
		
		window.dataRoles = {
			items: {!! json_encode($items) !!},
			permissionList: {!! json_encode($permissionList) !!}
		};

	    modules = ['xeditable','ngTable'];
	</script>
@stop
@section('script')
	 @if(!isProduction() && !isDev())
		{!! Html::script('app/components/role/roleController.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/role/roleService.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/role/roleDirective.js?v=getVersionScript()')!!}
	@else
	    <script src="{{ elixir('app/pages/role.js') }}"></script>
	@endif
@stop