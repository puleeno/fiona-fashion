@extends('app')
@section('title') Roles | @stop
@section('content')
<div data-ng-controller="RoleUserController">
	<ol class="breadcrumb breadcrumb-permission"> 
		<li><a href="http://localhost:8000/admin/user/roles">Manage Roles</a></li> 
		<li>Role Editor</li> 
	</ol>
	<div class="wrap-permission-user">
		<div class="col-lg-offset-1 col-lg-10" style="background:#fff;padding:20px">
			<div class="form-group">
				<label class="col-lg-2 label-title-edit">Role Name :</label>
				<div class="col-lg-4">
					<div class="input-group">
					  	<input class="form-control" id="appendedInputButtons" type="text" placeholder="Placeholder .." value="{{$role['name']}}">
					  	<div class="input-group-btn">
					  		<button class="btn btn-primary" type="button" style="border-radius:0px;"><i class="fa fa-check"></i></button>
					  	</div>
					  	<div class="input-group-btn">
					  		<button class="btn btn-default" type="button"><i class="fa fa-times"></i></button>
					  	</div>
					</div>
				</div>
				
				<div class="clearfix"></div>
			</div>
			<div class="form-group">
				<label class="col-lg-2 label-title-edit">Role Slug : </label>
				<div class="col-lg-10">
					<a href="">{{$role['display_name']}}</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="form-group">
				<label class="col-xs-6 col-lg-6 form-group">Role Decription : </label>
				<div class="col-xs-6 col-lg-6 text-right form-group">
					<a href="" class="btn btn-primary btn-xs"><i class="fa fa-check"></i></a>
					<a href="" class="btn btn-default btn-xs"><i class="fa fa-times"></i></a>
				</div>
				<div class="col-lg-12 form-group">
					<textarea name="" id="" cols="30" rows="5" class="form-control">{{$role['description']}}</textarea>
				</div>
				
				<div class="clearfix"></div>
			</div>
			<div class="col-lg-6 box-w-m">
		        <div class="box-role">
		            <h5 class="margin-none title-box-r">Roles</h5>
		            <div id="box-role-fix">
		                <div class="w-item">
		                    <span class="item pull-left">Admin user</span>
		                    <span class="action pull-right">
		                    <i class="fa fa-arrow-circle-o-right"></i>
		                    </span>
		                    <div class="clearfix"></div>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="col-lg-6 box-w-m" >
		        <div class="box-role">
		            <h5 class="margin-none title-box-r">Assinees</h5>
		            <div  id="box-assignee-fix">
			            <div class="w-w-item toggle-role-p">
			                <div class="w-item">
			                    <span class="item toggle-role pull-left">Admin user</span>
			                    <span class="action pull-right">
			                    <i class="fa fa-trash"></i>
			                    </span>
			                    <div class="clearfix"></div>
			                </div>
			            </div>
		            </div>
		        </div>
		    </div>
			<div class="col-lg-12" style="margin-top:20px;">
				<div class="table-responsive table-action-user"> 
					<table class="table" style="min-width:320px;">
						<thead>
							<th>User</th>
							<th class="text-right action-add-u">
								<a ng-click="open=true" class=""><i class="fa fa-user"></i><i class="fa fa-plus"></i></a>
								<a href="" class=""><i class="fa fa-group"></i><i class="fa fa-plus"></i></a>
							</th>
						</thead>
						<tbody>
							<tr ng-if="open">
								<td colspan="3">
								<search type="false" items="users" ng-model="role.user_id" user-id="{{Auth::user()->id}}" placeholder="Select User" on-change="changUser()"></search>
								</td>
							</tr>
							<tr ng-repeat="item in users_role">
								<td>
									<i class="fa fa-user"></i>
									<a href="" style="margin-left:50px;">@{{item.first_name}} @{{item.last_name}}</a>
								</td>
								<td>
									<a  ng-click="delete(item)"><i class="fa fa-trash pull-right"></i></a>
								</td>
							</tr>
							<tr>
								<td>
									<i class="fa fa-group"></i>
									<a href="" style="margin-left:50px;">Jacob Sparkkkkk</a>
								</td>
								<td>
									<i class="fa fa-trash pull-right"></i>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="modal" id="show_list" aria-hidden="false">
		
		<div class="modal-dialog">
			<div class="modal-content">

				<!-- Modal heading -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="modal-title">Assignee</h3>
				</div>
				<!-- // Modal heading END -->
				
				<!-- Modal body -->
				<div class="modal-body">
					<div class="w-w-item"> 
						<div class="w-item"> 
							<span class="item" style="font-weight:700">Supper Admin (Role)</span> 
						</div> 
						<div class="item-ch"> 
							<span>-</span> <span>Supper Admin</span> 
						</div> 
					</div>
				</div>
				<!-- // Modal body END -->
				
				<!-- Modal footer -->
				<div class="modal-footer">
					<a href="#" class="btn btn-default" data-dismiss="modal">Close</a> 
				</div>
				<!-- // Modal footer END -->

			</div>
		</div>
	</div>
</div>
@stop
@section('scripts-modules')
	<script>
	    modules = ['xeditable','ngTable'];
	    window.users_role = {!!json_encode($users_role)!!};
	    window.role_id = {!!json_encode($id)!!};
	</script>
@stop
@section('script')
	@if(!isProduction() && !isDev())
		{!! Html::script('app/components/role/roleuserController.js?v=getVersionScript()')!!}
		{!! Html::script('app/shared/search/searchDirective.js')!!}
		{!! Html::script('app/components/role/roleService.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/role/roleuserService.js?v=getVersionScript()')!!}
		{!! Html::script('app/components/role/roleDirective.js?v=getVersionScript()')!!}
	@else
	    <script src="{{ elixir('app/pages/role.js') }}"></script>
	@endif
@stop