@extends(getapp())
@include('users::shared.head')
@section('content')
<div ng-app="users-rowboat" data-ng-controller="UserController">
 	<ol class="breadcrumb">
       <li><a href="{{URL::to('admin/user')}}">Manage Users</a></li>
       <li>Update Permissions For <strong>{{$user->first_name}} {{$user->last_name}}</strong> </li>
    </ol>

	<div class="clearfix"></div>
	<div class="wrap-permission-user">
		<h4 class="title-multiselect">Roles</h4>
		<multi-select placeholder="Roles" items="listRoles" items-assigned="userRoles" on-change="updateRole()"> </multi-select>
		<h4 class="title-multiselect">Permissions</h4>
		<multi-select items="listPermissions" items-assigned="userPermissions" on-change="updatePermission()"> </multi-select>
	</div>
</div>
@include('users::shared.script')
	<script  type="text/javascript">
	    window.dataController = {
	    	'id': {{$id}},
	    	'listPermissions' : {!!json_encode($listPermissions)!!},
	    	'userPermissions' : {!!empty($userPermissions)? '{}': json_encode($userPermissions)!!},
	    	'listRoles'       : {!!json_encode($listRoles)!!},
	    	'userRoles'       : {!!empty($userRoles)? '{}': json_encode($userRoles)!!}
	    }
	</script>
@stop
@section('script')
	{!! Html::script('app/components/user/userController.js')!!}
	{!! Html::script('app/components/user/userService.js')!!}
	{!! Html::script('app/shared/multi-select/multiSelectDirective.js')!!}
	
@stop
	