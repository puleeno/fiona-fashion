<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '716374128444280',
        'client_secret' => '68c928277c7812dbbdbd78ae30b589d2',
        'redirect' => 'http://fiona.app/callback/fb',
    ],

    'google' => [
        'client_id' => '126658220975-6ro2q7abb7p1gcot8j8csifldhq2657k.apps.googleusercontent.com',
        'client_secret' => '7uQj2JVjLbnXHCynS4dJ8OJw',
        'redirect' => 'http://fiona.app/callback/gg',
    ],

];
