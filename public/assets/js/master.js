var app = angular.module('masterApp', []);
app.controller('MasterController', ['$scope', '$uibModal', '$filter', '$rootScope', 'ngDialog', '$http',
    function ($scope, $uibModal, $filter, $rootScope, ngDialog, $http) {
    $rootScope.$on("CallToMethodShowCart", function(event, data){
        $scope.showCart(data);
    });

    $scope.showCart = function(data) {
        $scope.carts = data.carts;
        $scope.priceTotal = data.priceTotal;
        $scope.numberItem = data.numberItem;
    }

    $scope.carts = window.carts;
    $scope.numberItem = window.numberItem;
    $scope.isActive = false;
    
    $scope.viewCarts = function(){
        $scope.isActive = !$scope.isActive;
        $('.gio-hang').addClass('active-cart');
        ngDialog.open({
            template: '/assets/views/viewCart.html',
            className: 'ngdialog-theme-default dialog-product-cart',
            scope: $scope
        });
        return false;
    }
    

}]);
