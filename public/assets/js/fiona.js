var defaultModules = [
                        'ui.bootstrap',
                        'ngResource',
                        'ngDialog',
                        'CartApp',
                        'masterApp',
                        'CustomerApp'
                    ];

if(typeof modules != 'undefined'){
    defaultModules = defaultModules.concat(modules);
}

app = angular.module('fiona', defaultModules);

app.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
});

var customerApp = angular.module('CustomerApp', []);
customerApp.factory('CustomerResource',['$resource', function ($resource){
    return $resource('/api/customer/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('CustomerService', ['CustomerResource', '$q', function (CustomerResource, $q) {

    /**
     * Create new customer
     * @author  Thanh Tuan <tuan@httsolution.com>
     * @param  {Object} data Data input
     * @return {Void}      
     */
    this.createCustomerProvider = function(data){
        var defer = $q.defer(); 
        var temp  = new CustomerResource(data);
        /* Create customer successfull */
        temp.$save({}, function success(data) {
            /* Resolve result */
            defer.resolve(data);
        },
        /* If create customer is error */
        function error(reponse) {
            /* Resolve result */
            defer.resolve(reponse.data);
        });

        return defer.promise;  
    };

    /**
     * Login customer
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} data Data input
     * @return {Void}      
     */
    this.login = function (data) {
        var defer = $q.defer(); 
        var temp  = new CustomerResource(data);
        temp.$save({method: 'login'}, function success(data) {
            defer.resolve(data);
        },
        
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    /**
     * Send Email to Customer
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Object} data Customer
     * @return {Void}      
     */
    this.sendEmail = function (data) {
        var defer = $q.defer(); 
        var temp  = new CustomerResource(data);
        temp.$save({method: 'send-email'}, function success(data) {
            defer.resolve(data);
        },
        
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

    this.loginFb = function (data) {
        delete data.id;
        var defer = $q.defer(); 
        var temp  = new CustomerResource(data);
        temp.$save({method: 'login-facebook'}, function success(data) {
            defer.resolve(data);
        },
        
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;  
    }

}]);

var app = angular.module('CartApp', []);
app.factory('CartResource',['$resource', function ($resource){
    return $resource('/api/shopping-cart/:method/:id', {'method':'@method','id':'@id'}, {
        add: {method: 'post'},
        save:{method: 'post'},
        update:{method: 'put'}
    });
}])
.service('CartService', ['CartResource', '$q', function (CartResource, $q) {
    /**
     * Add product to cart
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param {String} productId Id of product
     */
    this.addProductToCart = function(productId){
        $('#page-loading').css('display', 'block');
        var defer = $q.defer();
        var temp  = new CartResource();
        temp.$save({id: productId}, function success(data) {
            defer.resolve(data);
        },
        function error(reponse) {
            defer.resolve(reponse.data);
        });

        return defer.promise;
    };

    /**
     * Update cart
     * @author Thanh Tuan <thanhtuancr2011@gmail.com>
     * @param  {Cart} data Cart
     * @return {Void}
     */
    this.updateCart = function(data){
        var defer = $q.defer();
        var temp  = new CartResource(data);
        temp.$update({id: data['rowid']}, function success(data) {
            defer.resolve(data);
        },
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;
    };

    this.deleteCart = function (id) {
        var defer = $q.defer();
        var temp  = new CartResource();
        temp.$delete({id: id}, function success(data) {
            defer.resolve(data);
        },
        function error(reponse) {
            defer.resolve(reponse.data);
        });
        return defer.promise;
    }

}]);

app.controller('MainCtrl', ['$scope', '$uibModal', '$filter', '$timeout', 'ngDialog', '$http', 'CartService', '$rootScope', 'CustomerService',
    function ($scope, $uibModal, $filter, $timeout, ngDialog, $http, CartService, $rootScope, CustomerService) {
    $scope.open = function () {

        $scope.login = {};
        $scope.loginUser = function(validate) {
            $scope.msg = '';
            $scope.submitted = true;
            if(validate){
                return;
            }
            CustomerService.login($scope.login).then(function (data){
                if (data.status == 0) {
                    $scope.msg = data.msg;
                } else {
                    window.location = window.baseUrl;
                }
            })
        }

        $scope.openCalendar = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.open_calendar = true;
        };

        $scope.register = {};
        $scope.registerUser = function(validate) {

            $scope.emailExists = '';
            $scope.messageCreateSuccess = '';

            $scope.submitted = true;

            if (validate) {
                return;
            }

            if (angular.isDefined($scope.register.birth_date) && $scope.register.birth_date != '') 
                    $scope.register.birth_date = $filter('date')(new Date($scope.register.birth_date), 'yyyy-MM-dd');

            if ($scope.register.birth_date == '1970-01-01') $scope.register.birth_date = '';
            
            CustomerService.createCustomerProvider($scope.register).then(function (data){
                if(data.status == 0){
                    $scope.emailExists = data.error.email[0];
                } else{
                    $scope.register = {};
                    $scope.submitted = false;
                    $scope.messageCreateSuccess = 'Chúc mừng! tài khoản của bạn đã được tạo thành công.';
                    $timeout(function(){
                        $scope.messageCreateSuccess = '';
                        window.location = window.baseUrl;
                    }, 3000)
                }
            });
        }

        ngDialog.open({
            template: '/assets/views/loginPopup.html?v=' + new Date().getTime(),
            className: 'ngdialog-theme-default dialog-popup-login',
            scope: $scope
        });
        return false;
    };

    $scope.viewProduct = function($product_id){

        $http.get(fiona_url + '/product/' + $product_id).success(function(data){
            $scope.product = data;
            $scope.addProductToCart = function (productId) {
                CartService.addProductToCart(productId).then(function(data) {
                    $rootScope.$emit("CallToMethodShowCart", data);
                });
            }
        });
        ngDialog.open({
            template: '/assets/views/viewProduct.html',
            className: 'ngdialog-theme-default dialog-product-detail',
            scope: $scope
        });
        return false;
    }

    /**
     * When user click update cart
     * @param  {Object} cart Cart
     * @return {Void}
     */
    $scope.updateCart = function(cart) {
        CartService.updateCart(cart).then(function(data) {
            $scope.carts = data.carts;
            $scope.priceTotal = data.priceTotal;
            $scope.numberItem = data.numberItem;
            $rootScope.$emit("CallToMethodShowCart", data);
        });
    }

    /**
     * When user click delete cart
     * @param  {String} rowId Cart id
     * @return {Void}
     */
    $scope.deleteCart = function(rowId) {
        CartService.deleteCart(rowId).then(function(data) {
            $scope.carts = data.carts;
            $scope.priceTotal = data.priceTotal;
            $scope.numberItem = data.numberItem;
            $rootScope.$emit("CallToMethodShowCart", data);
        });
    }

}]);
