<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Store;
use App\Models\Location;

class StoreController extends BaseContorller
{
    public function index()
    {
        $stores = Store::all();
        $locations = Location::all();
    	return view('store.index', compact('stores', 'locations'));
    }

    public function post(){

    }
}
