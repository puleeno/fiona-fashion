<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Services\SocialAccountService;
use Socialite;
use Auth;

class SocialAuthController extends BaseContorller
{
	public function index()
	{
		return view('auth.login');
	}

    public function redirectFB()
    {
        return Socialite::driver('facebook')->redirect();   
    }   

    public function callbackFB(SocialAccountService $service)
    {
    	$customer = $service->createOrGetUser(Socialite::driver('facebook')->user(), 'facebook');
        
        Auth::guard('customer')->login($customer);

        return redirect()->to('/');
    }

    public function redirectGG()
    {
        return Socialite::driver('google')->redirect();   
    }   

    public function callbackGG(SocialAccountService $service)
    {
        $customer = $service->createOrGetUser(Socialite::driver('google')->user(), 'google');
        
        Auth::guard('customer')->login($customer);

        return redirect()->to('/');
    }
}
