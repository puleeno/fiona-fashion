<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Comus\Core\Models\CategoryModel;

class HomeController extends BaseContorller
{
    public function index()
    {
    	// d(\Auth::guard('customer')->user());die;
        $categories = CategoryModel::where('parent_id', 1)
                        ->paginate(4);
        $category_count = $categories->count();
    	return view('home', compact('categories', 'category_count'));
    }
}
