<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Http\Requests;
use \Comus\Core\Models\CategoryModel;

class BaseContorller extends Controller
{
    public function __construct()
    {
    	$product_cats = new \Comus\Core\Models\CategoryModel();
    	view()->share('product_cats', $product_cats);
    }
}
