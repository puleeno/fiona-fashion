<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \Comus\Core\Models\ProductModel;
use \Comus\Core\Models\PhotoModel;

class ProductController extends BaseContorller
{
    public function index(){
        $products = ProductModel::paginate(12);
        return view('product.index', compact('products'));
    }

    public function promotion(){
        $products = ProductModel::where('old_price', '>', 0)
                                ->paginate(12);
        return view('product.index', compact('products'));
    }

    public function detail($product_alias){
        $product = ProductModel::where('alias', $product_alias)
                                ->first();
        $discount = 0;
        if($product->old_price > 0 &&  $product->old_price > $product->price){
            $discount = $product->old_price - $product_price;
            $discount = $discount / $product->old_price;

        }
        return view('product.detail', compact('product', 'discount'));
    }

    public function show($product_id){
        $product = ProductModel::where('id', $product_id)->first();
        if($product->old_price > 0 && $product->price < $product->old_price){
            $product->discount = round(100 - $product->price / $product->old_price * 100);
        }
        $product->images = $product->images()->get();
        return response()->json($product);
    }
}
