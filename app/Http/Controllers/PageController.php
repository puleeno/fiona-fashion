<?php
namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Comus\Core\Models\ArticleModel;

class PageController extends BaseContorller
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function index($page_name){
        $page = ArticleModel::where('alias_title', $page_name)
                            ->where('type', 'page')
                            ->where('status', 'published')
                            ->first();
        $page_nav = ArticleModel::where('type', 'page')
                                ->where('status', 'published')->get();
        if($page){
            return view('page.index', compact('page', 'page_nav'));
        }
        else{
            abort('404');
        }
    }
}
