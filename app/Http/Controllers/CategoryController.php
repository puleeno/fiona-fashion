<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \Comus\Core\Models\CategoryModel;

class CategoryController extends BaseContorller
{
    public function show($cat_alias) {
    	$cat = CategoryModel::where('alias', $cat_alias)
    							->first();
    	return view('category.index', compact('cat'));
    }
}
