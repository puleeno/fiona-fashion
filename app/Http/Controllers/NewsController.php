<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Comus\Core\Models\ArticleModel;

class NewsController extends BaseContorller
{
    public function index()
    {
        $articles = ArticleModel::where('status', 'published')
                                ->where('type', 'article')
                                ->paginate(5);
        $total_articles = $articles->count();
        $start_index = 0;
    	return view('news.index', compact('articles', 'total_articles', 'start_index'));
    }

    public function detail($alias_title){
        $article = ArticleModel::where('status', 'published')
                                ->where('type', 'article')
                                ->where('alias_title', $alias_title)
                                ->first();
        $related_articles = ArticleModel::where('id', '!=' , $article->id)
                                        ->where('type', 'article')
                                        ->take(5)
                                        ->get();
        if($article){
            return view('news.detail', compact('article', 'related_articles'));
        }
        else{
            abort('404');
        }
    }
}
