<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'HomeController');
Route::resource('/product', 'ProductController');
Route::get('/cua-hang.html', 'StoreController@index');
Route::get('/bo-suu-tap.html', 'CollectionController@index');
Route::get('/dang-ky.html', 'UserController@index');
Route::get('/dang-nhap.html', 'UserController@index');
Route::get('/gio-hang.html', 'CartController@collection');
Route::get('/thanh-toan.html', 'CheckoutController@collection');
Route::get('/thanks.html', 'ThanksController@collection');
Route::get('/tin-tuc.html', 'NewsController@index');
Route::get('/tin-tuc/{news_title}.html', 'NewsController@detail');
Route::get('/san-pham.html', 'ProductController@index');
Route::get('/khuyen-mai.html', 'ProductController@promotion');
Route::get('/danh-muc/{cat_name}.html', 'CategoryController@show');
Route::get('/san-pham/{product_name}.html', 'ProductController@detail');
Route::get('/{page}.html', 'PageController@index');
Route::group(['middleware' => ['web']], function () {
	route::get('/login', 'SocialAuthController@index');
	Route::get('/redirect/fb', 'SocialAuthController@redirectFB');
	Route::get('/callback/fb', 'SocialAuthController@callbackFB');
	Route::get('/redirect/gg', 'SocialAuthController@redirectGG');
	Route::get('/callback/gg', 'SocialAuthController@callbackGG');
});

Route::group(['prefix' => 'api'], function(){
	/* Route cart */
	Route::post('shopping-cart/{id}', 'CartController@addProductToCart');
	Route::resource('shopping-cart', 'CartController');

	/* Route customer */
	Route::post('customer/send-email/{id}', 'CustomerController@sendEmailToCustomerPurchase');
	Route::post('customer/login', 'CustomerController@postLogin');
	Route::resource('customer', 'CustomerController');
});
