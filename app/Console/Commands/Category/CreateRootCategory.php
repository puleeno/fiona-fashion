<?php

namespace App\Console\Commands\Category;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

use Comus\Core\Models\CategoryModel;

class CreateRootCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cmd:create_root_category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create root category';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start...');

        $data = [
            'name' => 'Root category',
            'parent_id' => 0,
            'sort_order' => 0,
            'ancestor_ids' => "0",
            'keywords' => 'root category',
            'description' => 'Root category',
            'alias' => 'root_category'
        ];

        $category = CategoryModel::create($data);

        $this->info('Success...');
    }
}
